package com.example.tipcalculatoractivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception
import kotlin.math.round

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        calc_button.setOnClickListener{handleClick()}

    }
    private fun handleClick()
    {try {
        val enteredText=input_text.editableText.toString()
        //added
        val convertType = when(Radio_Group.checkedRadioButtonId) {
            R.id.rb1 -> ConvertType.p10
            R.id.rb2 -> ConvertType.p20
            R.id.rb3 -> ConvertType.p30
            else -> throw Exception()
        }

        val enteredValue = enteredText.toDouble()

        val result = convertTip(enteredValue, convertType)

        val final_result=result+enteredValue
        result_tv.text = resources.getString(R.string.tip_string)+result.toString()+resources.getString(R.string.total_string)+final_result.toString()
    }
    catch(e: Exception)
    {
        result_tv.text=resources.getString(R.string.error_string)

    }

    }

}
enum class ConvertType{ p10, p20,p30 }

fun convertTip(input: Double, convertType: ConvertType): Double {
    val result = when(convertType) {
        ConvertType.p10-> input*.10
        ConvertType.p20 -> input *.20
        ConvertType.p30 -> input *.30
    }
    return result.round(2)
}

private fun Double.round(decimals: Int): Double {
    var multiplier = 1.00
    repeat(decimals) { multiplier *= 10 }
    return round(this * multiplier) / multiplier
}